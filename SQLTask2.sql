DELIMITER //
create procedure ranking()
begin 

/* First table displaying the Date and the three biggest ranges between the open and close price*/
create temporary table DateRange

select cast(max(abs(open-close)) as DECIMAL(4,2)) as maxhigh,`date` 
from sd3
group by `date` 
order by 1 DESC limit 3; 

/* Second table displying the Date and the MaxHigh */
create temporary table DateMaxHigh

select `date`, max(high) as maxhigh
from sd3
where `date` in  (select `date` from DateRange)
group by `date` 
order by 1 ASC limit 3;



/* Third table displaying the Date and the Time the MaxHigh occured */
create temporary table MaxHighTime

select sd3.Date, DATE_FORMAT(str_to_date(sd3.Time, "%H%i"),"%H:%i") as MaxHigh 
from sd3
join DateMaxHigh mh on sd3.date = mh.date
where sd3.High = mh.maxhigh;


/* Final Table */
select DateRange.Date, DateRange.maxhigh as `Range`, MaxHighTime.MaxHigh as TimeMax
from DateRange 
join MaxHighTime on DateRange.date = MaxHighTime.date;


drop table DateRange;
drop table DateMaxHigh;
drop table MaxHighTime;


end //

DELIMITER;
