# **SQL and Python Challenges**  

These challenges will require either MySQL or SQL Server. The aim of the challenges is to understand 
how to analyse data and to compose complex SQL queries.

## Challenge 1 - CALCULATING VOLUME Weighted Prices (MySQL) ##

## Assumptions 
Use the close price as the trade price. 

## SQL used to change the column headings and create a new table called sd2a  


create table sd2a  
select `<ticker>` as ticker, `<date>` as date, `<open>` as open, `<high>` as high, 
`<low>` as low, `<close>` as close, `<vol>` as vol   
from sd2;

## Script file containing store procedure
SQLTask1.sql

## Example to run on procedure 

`call sp_task1(201010110900, 201010111400);`

- - - 

## Challenge 2 - IDENTIFYING THE BIGGEST DAILY TRADING RANGES ##

## Assumptions 
* Use the open price minus the close price to find out the range. 

* Create a table called sd3

## Steps needed
Create 3 temporary tables inside the store procedure.

* First table (DateRange) which displays the Date and the three biggest ranges between the open and close price.

* Second table (DateMaxHigh) which displys the Date and the MaxHigh.

* Third table (MaxHighTime) which displays the Date and the Time the MaxHigh occured.

## Script file containing store procedure
SQLTask2.sql

## How to run it 

`call ranking;`


- - -
## Challenge 3 - TEXT FILE MANIPULATION (Python) ##

## Files
Under .gitignore the following files are present:

* hosts.real

* IP.txt --> it contains just the extracted IP addresses

* newformat.txt --> which contains the maiin file without any comments

## Python file containing all the code
PythonTask1.py

## How to run it

On the command line type: `python PythonTask1.py`

This will show the 10 MAC addresses only.


- - - 

## Challenge 4 - FEED HANDLER LOG MANIPULATION (Python) ##


## Files
Under .gitignore the following files are present:

* opra_example_regression.log --> this file is very large 


## Python file containing all the code
PythonTask2.py


## How to run it

On the command line type: `python PythonTask2.py`

This will show the Trade Types which are under Regression and it will show show the TradePrice and TradeVolume.


## Extra command 

On the command line type: `python PythonTask2.py | grep "Type: Trade" | wc -l ` and normally the output 
should be 95. 


- - -

## Authors

* **Natalia Christofi**  



