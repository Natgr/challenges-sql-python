import re

fobjr=open("hosts.real","r")
fobjw=open("newformat.txt","w")
fobjw2=open("IP.txt","w")

for line in fobjr:
    if not line.startswith("#"):
        data=re.split("\s+",line)
        fobjw2.write(data[0]+"\n")

        try:
            keepdata=data[0:data.index("#")]

        except:
            keepdata=data
        fobjw.write((" ".join(keepdata)+"\n"))

    reip=re.compile("(?:[0-9a-fA-F]:?){12}")

    ip=reip.search(line)
    if ip != None:
        print(ip.group(0))
    print("\n")

fobjr.close()
fobjw.close()
