DELIMITER //

create procedure sp_task1(startdate varchar(12), enddate varchar(12))
begin
	set @startdate=startdate;
    set @enddate=enddate; 

	SELECT cast(SUM(vol*close)/SUM(vol) as DECIMAL(11,8)) as VWAP, 
	DATE_FORMAT(str_to_date(@startdate,"%Y%m%d"), "%d/%m/%Y") as `Date`,
	CONCAT("Start (", DATE_FORMAT(str_to_date(@startdate, "%Y%m%d%H%i"),"%H:%i"), ") - End (",
	DATE_FORMAT(DATE_ADD(str_to_date(@startdate,"%Y%m%d%H%i"), interval 5 hour), "%H:%i"), ")") as `Interval`
	FROM sd2a
	WHERE `date` BETWEEN @startdate AND @enddate;

end //

DELIMITER;

