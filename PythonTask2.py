import re

fobjr=open("opra_example_regression.log","r")

recpubre=re.compile("Record Publish:")
ttre=re.compile("Type: Trade")
tvre=re.compile("wTradeVolume")
tpre=re.compile("wTradePrice")

recpub=tt=tv=tp=""
for line in fobjr:
    if recpubre.search(line):
        recpub=line
        tt=tv=tp=""
    if ttre.search(line):
        tt=line

    if recpub and tt:
        if tvre.search(line):
            tv = line
        if tpre.search(line):
            tp = line
        if recpub and tt and tv and tp:
            recpub=re.sub("^Regression:\s+","",recpub)
            tt=re.sub("^Regression:","",tt)
            tv=re.sub("^Regression:","",tv)
            tp=re.sub("^Regression:","",tp)
            print(recpub.rstrip("\n"),tt,tv,tp)
            recpub=tt=tv=tp=""

fobjr.close()
